---
title: "invest5m178_flexdashboard"
runtime: shiny
output: 
  flexdashboard::flex_dashboard:
    orientation: columns
    vertical_layout: fill
---

```{r setup, include=FALSE}

library(flexdashboard)
library(googledrive)
library(googlesheets4)
library(shinyWidgets)
library(plotly)
library(dplyr)
library(tidyr)
library(magrittr)
library(ggplot2)
library(ggforce)
library(lubridate)
library(stringr)
library(scales)
library(tidyquant)
library(kableExtra)
library(DT)


# library(jsonlite)
# library(odbc)
# library(DBI)

# con_snow <- dbConnect(odbc(),
#                  Driver = "SnowflakeDSIIDriver",
#                  Server = read_json('snow.json')$server,
#                  uid = 'HELENZHANGXJ',
#                  pwd = read_json('snow.json')$pw,
#                  warehouse = 'COMPUTE_WH',
#                  database = "DB001")


```



```{r}

gs4_deauth()

ticker_daily_df <- read_sheet('1oXmAMnVQyQJd7GZ2loEHoFeve7tTwa32xpqa4SCPk-w', sheet = 'ticker_daily') %>% 
  mutate(date = as.Date(date)) %>%
  arrange(symbol, date)

fx_df <- read_sheet('1oXmAMnVQyQJd7GZ2loEHoFeve7tTwa32xpqa4SCPk-w', sheet = 'fx_daily') %>%
  transmute(date = as.Date(Date), 
            fx_close = round(Close, 4)) %>%
  arrange(date)

ticker_index <- read_sheet('1oXmAMnVQyQJd7GZ2loEHoFeve7tTwa32xpqa4SCPk-w', sheet = 'index_close')

journals <- read_sheet('1oXmAMnVQyQJd7GZ2loEHoFeve7tTwa32xpqa4SCPk-w', sheet = 'journals')

deposit_transaction <- journals %>% 
  filter(account == 'deposit CNY' & str_starts(description, 'deposit')) %>%
  select(-symbol, -price, -qty)
  
ticker_transaction <- journals %>% filter(!is.na(symbol)) %>%
  select(-description)

ticker_bal <- data.frame()

for (s in unique(ticker_transaction$symbol)){
  
  date_grid_start_temp <- ticker_transaction %>% 
    filter(symbol == s) %>% 
    pull(date) %>% 
    min()
  
  date_grid_end <- Sys.Date()
  
  date_grid_temp <- seq.Date(from = as.Date(date_grid_start_temp), 
                             to = as.Date(date_grid_end), 
                             by = 'day')
  
  ticker_bal_temp <- data.frame(date = date_grid_temp) %>% 
    left_join(ticker_transaction %>% filter(symbol == s), by = 'date') %>%
    fill(c('currency', 'account', 'symbol'))
  
  ticker_bal <- bind_rows(ticker_bal, ticker_bal_temp)}


ticker_bal_daily <- ticker_bal %>% mutate(date = ymd(date, tz='Pacific/Auckland')) %>%
  mutate(date = with_tz(date, tz = 'America/New_York')) %>%
  mutate(date = as.Date(date)) %>%
  fill(c('currency', 'symbol'), .direction = 'down') %>% 
  
  left_join(ticker_daily_df, by = c('date', 'symbol')) %>%
  
  left_join(fx_df, by = 'date') %>%
  filter(!is.na(close)|!is.na(price)) %>%
  mutate(amount = ifelse(is.na(amount), 0, amount), 
         rate = ifelse(is.na(rate), fx_close, rate), 
         amount_cny = ifelse(is.na(amount_cny), amount * rate, amount_cny), 
         price = ifelse(is.na(price), close, price), 
         qty = ifelse(is.na(qty),0,qty))

ticker_bal_cum <- ticker_bal_daily %>% 
  arrange(symbol, date) %>%
  group_by(symbol) %>%
  mutate(cumsum_qty = cumsum(qty), 
         cumsum_cost = cumsum(amount),
         cumsum_close = cumsum_qty * close, 
         cumsum_cost_cny = cumsum(amount_cny), 
         cumsum_close_cny = cumsum_qty * close * fx_close)

```


```{r}

params_date_start <- min(ticker_daily_df$date)
params_date_end <- max(ticker_daily_df$date)

ticker_daily_overall <- ticker_bal_cum %>%
  select(date, symbol, cumsum_qty, cumsum_cost, cumsum_close, cumsum_cost_cny, cumsum_close_cny) %>%
  mutate(unrealised_gain_loss = round(cumsum_close - cumsum_cost, 1), 
         unrealised_gain_loss_percent = round(unrealised_gain_loss/cumsum_cost,3)*100, 
         unrealised_gain_loss_cny = round(cumsum_close_cny - cumsum_cost_cny, 1), 
         unrealised_gain_loss_cny_percent = round(unrealised_gain_loss/cumsum_cost_cny,3)*100) %>%
  group_by(date) %>%
  
  summarise(cumsum_cost = sum(cumsum_cost, na.rm = TRUE), 
            cumsum_close = sum(cumsum_close, na.rm=TRUE), 
            unrealised_gain_loss = sum(unrealised_gain_loss, na.rm = TRUE),
            unrealised_gain_loss_percent = unrealised_gain_loss/cumsum_cost,
            
            cumsum_cost_cny = sum(cumsum_cost_cny, na.rm = TRUE),
            cumsum_close_cny = sum(cumsum_close_cny, na.rm=TRUE), 
            unrealised_gain_loss_cny = sum(unrealised_gain_loss_cny, na.rm = TRUE),
            unrealised_gain_loss_cny_percent = unrealised_gain_loss_cny/cumsum_cost_cny,
            .groups = 'drop') %>%
  
  mutate(unrealised_gain_loss_daily = cumsum_close - lag(cumsum_close), 
         unrealised_gain_loss_percent_daily = unrealised_gain_loss_daily/lag(cumsum_close))


ticker_daily_byticker <- ticker_bal_cum %>%
  select(date, symbol, cumsum_qty, cumsum_cost, cumsum_close, cumsum_cost_cny, cumsum_close_cny) %>%
  mutate(unrealised_gain_loss = round(cumsum_close - cumsum_cost, 1), 
         unrealised_gain_loss_percent = round(unrealised_gain_loss/cumsum_cost,3)*100, 
         unrealised_gain_loss_cny = round(cumsum_close_cny - cumsum_cost_cny, 1), 
         unrealised_gain_loss_cny_percent = round(unrealised_gain_loss/cumsum_cost_cny,3)*100) %>%
  group_by(symbol, date) %>%
  arrange(symbol, date) %>%
  
  summarise(cumsum_cost = sum(cumsum_cost, na.rm = TRUE),
            cumsum_close = sum(cumsum_close, na.rm=TRUE), 
            unrealised_gain_loss = sum(unrealised_gain_loss, na.rm = TRUE),
            unrealised_gain_loss_percent = unrealised_gain_loss/cumsum_cost,.groups = 'keep') %>%
  
  mutate(unrealised_gain_loss_daily = cumsum_close - lag(cumsum_close), 
         unrealised_gain_loss_percent_daily = unrealised_gain_loss_daily/lag(cumsum_close))


```


```{r}

subtotal_df <- journals %>% 
  group_by(currency, account) %>%
  summarise(amount = round(sum(amount, na.rm = TRUE),2), 
            amount_cny = round(sum(amount_cny, na.rm = TRUE),2), 
            .groups = 'drop') %>%
  
  left_join(fx_df %>% filter(date == params_date_end) %>% 
              mutate(account = 'deposit stock'), 
            by = 'account') %>%
    
  left_join(ticker_daily_overall %>% 
              filter(date == params_date_end) %>% 
              select(cumsum_close, cumsum_close_cny) %>%
              mutate(account = 'stock'), 
            by = c('account')) %>%
  
  mutate(amt_close = case_when(account == 'stock' ~ cumsum_close,
                               TRUE ~ amount), 
         amt_close_cny = case_when(account == 'deposit stock' ~ amount * fx_close, 
                                  account == 'stock' ~ cumsum_close_cny, 
                                  TRUE ~ amount_cny), 
         row_order = ifelse(str_starts(account, 'fee'),1,0)) %>%
  arrange(currency, row_order)

deposit_total <- sum(deposit_transaction$amount)
stock_bal <- subtotal_df %>% 
  filter(str_detect(account,'stock')) %>% 
  pull(amt_close_cny) %>% 
  sum()

a <- ticker_daily_overall %>% filter(date == params_date_end) %>% pull(unrealised_gain_loss) %>% replace_na(0) 

b <- ticker_daily_overall %>% filter(date == params_date_end) %>% pull(unrealised_gain_loss_percent) %>% replace_na(0)

c <- ticker_daily_overall %>% filter(date == params_date_end) %>% pull(unrealised_gain_loss_daily) %>% replace_na(0)

d <- ticker_daily_overall %>% filter(date == params_date_end) %>% pull(unrealised_gain_loss_percent_daily)  %>% replace_na(0) 

e <- stock_bal - deposit_total

``` 



```{r}

DimTicker <- read_sheet('1oXmAMnVQyQJd7GZ2loEHoFeve7tTwa32xpqa4SCPk-w', sheet = 'dim_ticker')

ticker_daily <- read_sheet('1oXmAMnVQyQJd7GZ2loEHoFeve7tTwa32xpqa4SCPk-w', 
                                              sheet = 'ticker_daily_temp') %>% 
  mutate(date = as.Date(date)) %>%
  arrange(symbol, date)

DimTicker[,sapply(DimTicker,is.character)] <- sapply(DimTicker[,sapply(DimTicker,is.character)],iconv,"WINDOWS-1252","UTF-8")

end_date <- (ticker_daily %>% 
               filter(symbol == 'AAPL') %>% 
               pull(date) %>% max())

start_date <- (ticker_daily %>% 
               filter(symbol == 'AAPL') %>% 
               pull(date) %>% min())

# ticker_minmax_52 <- ticker_daily %>% 
#   filter(date >= start_date & date <= end_date) %>%
#   group_by(symbol) %>%
#   summarize(close_min = min(close), close_max = max(close), .groups = 'keep')

ticker_daily_return <- ticker_daily %>%
  filter(!is.na(close)) %>%
  group_by(symbol) %>% 
  arrange(symbol, date) %>%
  
  # daily return
  tq_transmute(select  = close, 
               mutate_fun = periodReturn, 
               period = "daily", 
               type = "arithmetic", 
               col_rename = 'return_daily') %>% 
  
  # return vs prior day(-5)
  left_join(
    
    ticker_daily %>% 
      group_by(symbol) %>%
      transmute(symbol, date, 
                return_weekly = round((close - lag(close,5))/lag(close,5),4)), 
    by = c('symbol', 'date')
  ) %>%
  
  # return vs prior day(-20)
  left_join(
    
    ticker_daily %>% 
      group_by(symbol) %>%
      transmute(symbol, date, 
                return_monthly = round((close - lag(close,20))/lag(close,20),4)), 
    by = c('symbol', 'date')) %>%
  
  # vs SPY return
  left_join(ticker_daily %>% 
              filter(symbol == 'SPY') %>% 
              arrange(symbol, date) %>% 
              tq_transmute(select  = close, 
                           mutate_fun = periodReturn, 
                           period = "daily", 
                           type = "arithmetic", 
                           col_rename = 'return_daily'), by = 'date') %>%
  transmute(symbol, date, 
            return_daily = round(return_daily.x,4),
            return_weekly = round(return_weekly,4), 
            return_monthly = round(return_monthly,4),
            return_SPY = round(return_daily.y,4)) %>%
  
  mutate(rel = round(return_daily/return_SPY,4))


# get ticker output

ticker_daily_output <- ticker_daily %>%
  group_by(symbol) %>% 
  arrange(symbol, date) %>% 

    mutate(ma10 = rollmean(close, k = 10, fill = NA, align ="right"),
         ma20 = rollmean(close, k = 20, fill = NA, align ="right"), 
         ma30 = rollmean(close, k = 30, fill = NA, align ="right"),
         ma60 = rollmean(close, k = 60, fill = NA, align ="right"),

         ema20 = tryCatch(EMA(close,20), error = function(err) NA),
         ema30 = tryCatch(EMA(close,30), error = function(err) NA),
         ema60 = tryCatch(EMA(close,60), error = function(err) NA), 
  
         rsi = tryCatch(RSI(close), error = function(err) NA),
         momentum = tryCatch(momentum(close,1), error = function(err) NA),
         
         roc = tryCatch(ROC(close,1), error = function(err) NA),
         ma5_volume = rollmean(volume, k = 5, fill = NA, align ="right"),
         ma50_volume = rollmean(volume, k = 50, fill = NA, align ="right")) %>% 
  
  #   mutate(close_above_ma10 = ifelse(close>ma10,1,0), 
  #          close_above_ma20 = ifelse(close>ma20,1,0),
  #        close_above_ma2060120 = ifelse(close>ma20 & close>ma60 & close>ma120,1,0),
  #        ma20_above_ma60120 = ifelse(ma20>ma120 & ma60>ma120,1,0),
  #        ma5_vol_rel = round(volume/ma5_volume,3),
  #        ma5_volyn = ifelse(volume > ma5_volume, 1, 0),
  #        ma50_volyn = ifelse(volume > ma50_volume, 1, 0)) %>% 
  # 
  # # left_join(ticker_minmax_52, by ='symbol') %>%
  # 
  # mutate(ma10_lag = lag(ma10, n = 1, default = NA),
  #        ma10_lead = lead(ma10, n =1, default = NA),
  #        ma20_lag = lag(ma20, n = 1, default = NA),
  #        ma20_lead = lead(ma20, n =1, default = NA),
  #        ma60_lag = lag(ma60, n =1, default = NA),
  #        ma60_lead = lead(ma60, n = 1, dafault = NA),
  #        ma120_lag = lag(ma120, n =1, default = NA),
  #        ma120_lead = lead(ma120, n = 1, dafault = NA)) %>%
  # 
  # mutate(c_s = (close-ema20)/ema20*100, 
  #        s_m = (ema20-ema60)/ema60*100,
  #        m_l = (ema60-ema120)/ema120*100) %>%
  # 
  ungroup() %>%
  
  left_join(ticker_daily_return, by =c('symbol', 'date')) %>%
  
  left_join(DimTicker, by = 'symbol') %>%
  mutate(sector = if_else(is.na(sector), 'N/A', sector), 
         date = as.Date(date))

```


```{r}

p1 <- ticker_daily_output %>% 
      arrange(symbol, date) %>%
          
      ggplot(aes(x=date)) +
      geom_line(aes(y = close), color = 'bisque4', linetype = 'dashed') +
      geom_line(aes(y = ma20),color = '#55752f',size = 0.9) + 
      geom_line(aes(y = ma30), color = '#f0bd27',size = 0.9) +
      geom_line(aes(y = ma60),color = '#e15759',size = 0.9) +
      
    facet_wrap_paginate(~ str_sub(paste(symbol,company, sep = '//'),1,20), scale = 'free_y', 
                          nrow = 4, ncol = 6) +
    theme_classic() +
    theme(strip.text = element_text(size=6),
            axis.text = element_text(size=8), 
          axis.title.x = element_blank(), axis.title.y = element_blank())
    
n_p1 <- n_pages(p1)-1

```



```{r}

p2 <- ticker_daily_output %>% 
      arrange(symbol, date) %>%
      filter(date >= end_date %m+% months(-3)) %>%
      group_by(symbol) %>%
      mutate(return_daily_cum = cumsum(return_daily), 
             return_SPY_cum = cumsum(return_SPY)) %>%
      ungroup() %>%
      select(symbol, date, sector, company, return_daily_cum, return_SPY_cum) %>%
        
      ggplot(aes(x = date, group = symbol)) +
      geom_line(aes(y = return_daily_cum),color = 'bisque4', size = 0.7) +
      geom_line(aes(y = return_SPY_cum), color = '#87bcbd', size = 0.7) +
      scale_y_continuous(labels=percent) +
      
      facet_wrap_paginate(~ str_sub(paste(symbol,company, sep = '//'),1,30), scale = 'free_y', 
                          nrow = 4, ncol = 6) +
      theme_classic() +
      theme(strip.text = element_text(size=6), 
            axis.text = element_text(size=8), 
            axis.title.x = element_blank(), axis.title.y = element_blank())

n_p2 <- n_pages(p2)-1

```



# main

Column {data-width=180}
-----------------------------------------------------------------------

### LTD unrealised +gain/-loss ($)

```{r}

# icon = ifelse(a>0, "fa-angle-down", "fa-angle-down"),

flexdashboard::valueBox(comma(a), color = ifelse(a>0,"success","warning"))


```

### LTD unrealised +gain/-loss (%)

```{r}

valueBox(sprintf('%.1f%%',b), color = ifelse(b,"success","warning"))

```

### Daily unrealised +gain/-loss ($)

```{r}

valueBox(comma(c), color = ifelse(c>0,"success","warning"))

```

### Daily unrealised +gain/-loss (%)

```{r}

valueBox(sprintf('%.1f%%',d), color = ifelse(d>0,"success","warning"))

```

### LTD unrealised +gain/-loss (CNY)

```{r}

valueBox(comma(e), color = ifelse(e>0,"success","warning"))

```

### 

_date refreshed: `r Sys.time()`_  
_for data upto: `r params_date_end`_ 

Column {data-width=350}
-----------------------------------------------------------------------

### Daily Close vs Input


```{r}

# label1 = `unrealised +gain/-loss`)

g1 <- ticker_daily_overall %>%
  ggplot(aes(x=date, label1 = unrealised_gain_loss)) +
  geom_line(aes(y=cumsum_cost), color = 'gray', linetype = 2) +
  geom_line(aes(y=cumsum_close)) +
  scale_y_continuous(labels = comma) +
  scale_x_date(date_labels = '%b-%y') +
  labs(x = NULL, y = 'amount ($)') +
  theme_classic()

ggplotly(g1)

```

### Monthly Return

```{r}

## mixed portfolio return vs SPY

g2 <- ticker_daily_overall %>% 
  mutate(date_month = floor_date(date, 'month')) %>%
  group_by(date_month) %>%
  summarise(date_end = max(date), .groups = 'drop') %>%
  inner_join(ticker_daily_overall, by = c('date_end' = 'date')) %>%
  arrange(date_month) %>%
  transmute(month = date_month,
            month_return = unrealised_gain_loss - lag(unrealised_gain_loss),
            month_cumsum_return = unrealised_gain_loss) %>% 
  ggplot(aes(x=month, y=month_cumsum_return)) +
  geom_line(color = 'gray', linetype = 2) +
  geom_col(aes(x=month, y = month_return)) +
  scale_y_continuous(labels = comma) +
  scale_x_date(date_labels = '%b-%y') +
  labs(x = NULL, y = 'returns ($)') +
  theme_classic()

ggplotly(g2)

  
```


Column {data-width=400}
-----------------------------------------------------------------------

### Symbol vs SPY Cumulative Return %

```{r}

ticker_return <- ticker_daily_byticker %>% 
                  select(symbol, date) %>%
                  left_join(ticker_daily_df, by = c('symbol','date')) %>%
                  transmute(symbol, date, close) %>%
                  group_by(symbol) %>% 
                  arrange(symbol, date) %>%
                  
                  # daily return
                  tq_transmute(select  = close, 
                               mutate_fun = periodReturn, 
                               period = "monthly", 
                               type = "arithmetic", 
                               col_rename = 'return_monthly') %>%
                  
                  left_join(ticker_index %>%
                              mutate(date= as.Date(date)) %>%
                              filter(symbol == 'SPY') %>% 
                              arrange(symbol, date) %>% 
                              tq_transmute(select  = close, 
                                           mutate_fun = periodReturn, 
                                           period = "monthly", 
                                           type = "arithmetic", 
                                           col_rename = 'return_monthly'), by = 'date') %>%
                  transmute(symbol, date, 
                            return_monthly = round(return_monthly.x,2),
                            return_SPY = round(return_monthly.y,2)) %>%
                  mutate(rel = round(return_monthly/return_SPY,2))

```



```{r}

g2 <- ticker_return %>% group_by(symbol) %>%
    mutate(return_monthly_cum = cumsum(return_monthly), 
           return_SPY_cum = cumsum(return_SPY)) %>%
    ungroup() %>%
    
    select(symbol, date, return_monthly_cum, return_SPY_cum) %>%
    
    ggplot(aes(x = date, group = symbol)) +
    geom_line(aes(y = return_monthly_cum)) +
    geom_line(aes(y = return_SPY_cum), color = '#87bcbd') +
    scale_y_continuous(labels=label_percent(accuracy = 0.1)) +
    scale_x_date(date_labels = '%b-%y') +
    facet_wrap(~ symbol, scale = 'free_y') +
    labs(x=NULL, y='cumulative return %\n ') +
    theme_classic()


ggplotly(g2)

```

# transactions


Column {data-width=300}
-----------------------------------------------------------------------

### Transactions Subtotal

```{r}

 subtotal_df %>%
  transmute(currency, account, 
            amount = format(amount, digits = 2, nsmall=2, big.mark=","), 
            amount_cny = format(amount_cny, digits = 2, nsmall=2, big.mark=","), 
            amt_close = format(amt_close, digits = 2, nsmall=2, big.mark=","), 
            amt_close_cny = format(amt_close_cny, digits = 2, nsmall=2, big.mark=",")) %>%
  kbl() %>%
  kable_styling()

```

<font size="2">_total deposit: `r format(deposit_total, digits = 2, nsmall=2, big.mark=",")`, stock account close balance in CNY: `r format(stock_bal, digits = 2, nsmall=2, big.mark=",")`_</font>

### USD/CNY exchange rate

latest rate: `r tail(fx_df$fx_close,1)`  

```{r}

g3 <- fx_df %>% filter(date >= params_date_start) %>%
  ggplot(aes(x=date, y=fx_close)) +
  geom_line() +
  scale_y_continuous(labels = comma) +
  scale_x_date(date_labels = '%b-%y') +
  labs(x = '\n', y = 'USD/CNY') +
  theme_classic()

ggplotly(g3)

```


Column {data-width=400}
-----------------------------------------------------------------------

### Transactions

```{r}

journals %>% transmute(date = as.Date(date), 
                       currency, 
                       amount = format(amount, digits = 2, nsmall=2, big.mark=","), 
                       amount_cny = format(amount_cny, digits = 2, nsmall=2, big.mark=","),
                       account, 
                       description = ifelse(is.na(description),'',description)) %>%
            arrange(desc(date)) %>% 
  kbl() %>% 
  kable_styling(latex_options = "HOLD_position")
  
```


```{r eval=FALSE, include=FALSE}

datatable(journals %>% transmute(date = as.Date(date), currency, 
                                 amount, amount_cny = round(amount_cny,2), account, description) %>%
            arrange(desc(date)),
          filter = 'top', 
          options = list(autoWidth = TRUE, 
                         columnDefs = list(list(className = 'dt-left', targets = '_all'))),
          rownames = FALSE)

```



```{r render subpages_01, include=FALSE}

# Create variable which stores all subpages outputs
out1 <- NULL

# Set knitr options to allow duplicate labels (needed for the subpages)
options(knitr.duplicate.label = 'allow')

# Create temporary environment which we use for knitting subpage.RMD 
subpage_env <- new.env()

assign("p1", p1, subpage_env)

for (n in c(1:n_p1)) {
  
  assign("n", n, subpage_env)
   
  p1_print <- p1 + 
    facet_wrap_paginate(~ str_sub(paste(symbol,company, sep = '//'),1,20), scale = 'free_y', 
                          nrow = 4, ncol = 6, page = n)
  
  assign("subpage_p1", p1_print, subpage_env)
  
  # Knit subpage.RMD using the subpage_env and add result to out vector
  out1 <- c(out1, knitr::knit_child('subpage_01.rmd', envir = subpage_env))

}

```

`r paste(out1, collapse = '')`


```{r render subpages_02, include=FALSE}

# Create variable which stores all subpages outputs
out2 <- NULL

# Set knitr options to allow duplicate labels (needed for the subpages)
options(knitr.duplicate.label = 'allow')

# Create temporary environment which we use for knitting subpage.RMD 
subpage_env <- new.env()

assign("p2", p2, subpage_env)

for (n in c(1:25)) {

  assign("n", n, subpage_env)
   
  p2_print <- p2 + 
    facet_wrap_paginate(~ str_sub(paste(symbol,company, sep = '//'),1,20), scale = 'free_y', 
                          nrow = 4, ncol = 6, page = n)
  
  assign("subpage_p2", p2_print, subpage_env)
  
  # Knit subpage.RMD using the subpage_env and add result to out vector
  out2 <- c(out2, knitr::knit_child('subpage_02.rmd', envir = subpage_env))

}

```

`r paste(out2, collapse = '')`
